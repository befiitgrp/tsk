<?php
require("connex.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Todo List</title>
<style>
body {
    font-family: Arial, sans-serif;
    margin: 0;
    padding: 0;
    background-color: #f4f4f4;
}

.container {
    max-width: 600px;
    margin: 50px auto;
}

#todotitle {
    text-align: center;
}

.form-container {
    margin-top: 20px;
}

form {
    margin-bottom: 20px;
}

form label {
    display: block;
    margin-bottom: 5px;
}

form input[type="text"] {
    width: 100%;
    padding: 8px;
    box-sizing: border-box;
}

button {
    padding: 8px 16px;
    cursor: pointer;
    transition: background-color 0.3s; /* Add smooth transition for background color change */
}

button:hover {
    background-color: #007bff; /* Change background color on hover */
    color: #fff; /* Change text color on hover */
}
#add{
    margin-left:90%;

}
#add:hover{
    background:  #007bff;

}
ul {
    list-style: none;
    padding: 0;
}

li {
    margin-bottom: 10px;
    padding: 10px;
    background-color: #ffffff;
    border: 1px solid #ddd;
    border-radius: 4px;
    display: flex;
    justify-content: space-between;
    align-items: center;
}

.done {
    background-color: #c3e6cb; /* Background color for completed tasks */
}

.not-done {
    background-color: #f8d7da; /* Background color for incomplete tasks */
}

form button {
    margin-left: 5px;
}

</style>
</head>
<body>
    <nav>        <h1 id="todotitle" class="text-center">Todo List</h1>
</nav>
    <div class="container">
        <div class="form-container">
            <form action="index.php" method="post">
                <div class="form-group">
                    <label for="title">Task</label>
                    <input type="text" name="title" id="title" class="form-control" placeholder="Task" required>
                    <button id="add" type="submit" name="action" value="new">Add</button>

                </div>
            </form>
        </div>
        <ul>
            <?php foreach ($taches as $tache): ?>
                <li class="<?php echo $tache['done'] ? 'done' : 'not-done'; ?>">
                    <?php echo htmlspecialchars($tache['title']); ?>
                    <form action="index.php" method="post">
                        <input type="hidden" name="id" value="<?php echo $tache['id']; ?>">
                        <button type="submit" name="action" value="toggle">Done</button>
                        <button id="delete" type="submit" name="action" value="delete">X</button>
                    </form>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</body>
</html>
